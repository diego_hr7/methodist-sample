
//
//  ViewController.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/25/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

class ViewController:UIViewController{
    override func viewDidLoad() {
        Connection.shared.getLocations(page: 1, paramSort: nil, isOrderAsc: nil) { (response, error) in
            debugPrint(response)
        }
    }
}
