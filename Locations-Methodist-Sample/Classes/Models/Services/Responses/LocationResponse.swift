//
//  LocationsResponse.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/25/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

struct LocationResponse:Decodable{
    var idLocation:Int
    var name:String
    var airportCode:String?
    var phoneNumber:String
    var textNumber:String
    var fleetType:String
    var multiCarDisplayName:String
    var openDate:String
    var closeDate:String?
    var gdsCode:String?
    var deliverable:Bool
    var deliverableDescription:[String:String]?
    var description:String
    var timeZone:String
    var bookable:Bool
    var hours:String
    var assetCode:String
    var address:Address?
    var curbside_detail:CurbsideDetail?
    
    
    private enum CodingKeys: String, CodingKey {
        case idLocation="id", name, airportCode="airport_code", phoneNumber="phone_number", textNumber="text_number", fleetType="fleet_type", multiCarDisplayName="multi_car_display_name", openDate="open_date", closeDate="close_date", gdsCode="gds_code", deliverable, deliverableDescription="deliverable_description", description, timeZone="time_zone", bookable, hours, assetCode="asset_code", address, curbside_detail
    }
    
    public static func initWithData(data:Data) -> ([LocationResponse]?, Error?){
        do {
            let jsonDecoder: JSONDecoder = JSONDecoder()
            let response = try jsonDecoder.decode([LocationResponse].self, from: data)
            return (response, nil)
            
        }catch let parseError{
            return (nil, parseError)
        }
    }
}
