//
//  CurbsideDtail.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/25/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

struct CurbsideDetail:Decodable{
    var active:Bool
    var description:String
    
    private enum CodingKeys: String, CodingKey {
        case active, description
    }
}
