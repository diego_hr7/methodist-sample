//
//  AddressLocation.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/25/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

struct Address:Decodable{
    var idAddress:Int
    var line1:String
    var line2:String?
    var city:String
    var state:String
    var zip:String
    var country:String
    var latitude:String
    var longitude:String
    
    private enum CodingKeys: String, CodingKey {
        case idAddress="id", line1, line2, city, state, zip, country, latitude, longitude
    }
}
