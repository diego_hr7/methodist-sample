//
//  Connection.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/25/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

class Connection{
    private typealias MethodAPI = String
    
    private let baseURL = "https://api.rac-tst.com/"
    private let baseHeaders = ["Api-Version":"2"]
    
    private enum methodsAPI:MethodAPI{
        case getLocations = "locations"
    }
    
    public static let shared:Connection = Connection()
    
    func getLocations(page:Int = 1, paramSort:String?,isOrderAsc: Bool? = true, completion: @escaping (completionHandler)){
        var urlStr = baseURL + methodsAPI.getLocations.rawValue + "?page=\(page)"
        if let paramSort = paramSort{
            urlStr.append("&sort=")
            if isOrderAsc == false{
                urlStr.append("-")
            }
            urlStr.append(paramSort)
        }
        guard let url = URL(string: urlStr)else{
            debugPrint("invalid url")
            return
        }
        debugPrint("URL:-----\(url)")
        var request = URLRequest(url: url)
        for headerKey in baseHeaders.keys{
            request.setValue(baseHeaders[headerKey], forHTTPHeaderField: headerKey)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, responsee, error) in
            if let errorConnection = error {
                completion(nil, errorConnection)
            } else {
                    if let data = data{
                        let responseObject = LocationResponse.initWithData(data: data)
                        if let locations = responseObject.0{
                            completion(locations, nil)
                        }else {
                            completion(nil, responseObject.1)
                        }
                        
                    }else{
                        completion(nil, NSError(domain: "", code: 100, userInfo: ["title":"Error parsing json"]))
                    }
                
            }
        }
        task.resume()
    }
}

extension Connection{
    public typealias completionHandler = (Decodable?, Error?) -> Void
}
