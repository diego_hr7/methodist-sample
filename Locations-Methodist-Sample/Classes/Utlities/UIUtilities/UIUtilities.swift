//
//  UIUtilities.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/26/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

class UIUtilities{
    static let shared = UIUtilities()
    
    private var containerView:UIView!
    public typealias actionSheetCompletionHandler = () -> Void
    
    static func showSimpleAlert(title:String?, message:String, controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        DispatchQueue.main.async {
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    static func showActionSheet( message:String, controller:UIViewController, lblOption1: String, completionOption1:@escaping actionSheetCompletionHandler, lblOption2: String, completionOption2:@escaping actionSheetCompletionHandler){
        let alertActionSheet = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: lblOption1, style: .default) { (action) in
            completionOption1()
        }
        let action2 = UIAlertAction(title: lblOption2, style: .default) { (action) in
            completionOption2()
        }
        alertActionSheet.addAction(action1)
        alertActionSheet.addAction(action2)
        alertActionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        DispatchQueue.main.async {
            controller.present(alertActionSheet, animated: true, completion: nil)
        }
    }
}


