//
//  LocationController.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/26/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

protocol LocationViewable {
    func displayLocations(locations:[LocationResponse])
    func showAlert(message:String)
}

class LocationController:UIViewController{
    
    @IBOutlet weak var tblLocations: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var locations:[LocationResponse] = []{
        didSet{
            if let table = self.tblLocations{
                table.reloadData()
            }
            
        }
    }
    
    private var refreshControl:UIRefreshControl = UIRefreshControl()
    private var currentPage:Int = 1
    private var isSortAsc:Bool = true
    private var paramSort:String = "id"
    private var locationPresenter:LocationPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationPresenter =  LocationPresenter(view: self)
        self.setupView()
        self.getFreshLocations()
    }
    private func setupView(){
        self.title = "Choose location"
        let nib = UINib(nibName: "LocationCell", bundle: nil)
        self.tblLocations.register(nib, forCellReuseIdentifier: LocationCell.cellIdentifier)
        self.refreshControl.addTarget(self, action: #selector(getFreshLocations), for: .valueChanged)
        self.tblLocations.addSubview(self.refreshControl)
        self.tblLocations.delegate = self
        self.tblLocations.dataSource = self
        self.searchBar.delegate = self
    }
    
    @IBAction func selectSort(_ sender: Any) {
        UIUtilities.showActionSheet(message: "Select a sort", controller: self, lblOption1: "by Id", completionOption1: {
            self.currentPage = 1
            self.locationPresenter?.getLocations(page:self.currentPage, paramSort: "id", isAsc: self.isSortAsc)
        }, lblOption2: "by Name") {
            self.currentPage = 1
            self.locationPresenter?.getLocations(page:self.currentPage, paramSort: "name", isAsc: self.isSortAsc)
        }
    }
    @IBAction func selectOrder(_ sender: Any) {
        self.currentPage = 1
        isSortAsc = !self.isSortAsc
        if let button = sender as? UIBarButtonItem{
            button.title = (self.isSortAsc) ? "Asc":"Des"
        }
        self.locationPresenter?.getLocations(page:self.currentPage, paramSort: paramSort, isAsc: self.isSortAsc)
    }
    
    
    
    @objc private func getFreshLocations(){
        currentPage = 1
        self.locations = []
        PKHUD.sharedHUD.show(onView: self.view)
        locationPresenter?.getLocations(page: currentPage, paramSort: nil, isAsc: nil)
    }
    @objc private func getMoreLocations(){
        if PKHUD.sharedHUD.isVisible{return}
        PKHUD.sharedHUD.show(onView: self.view)
        currentPage = currentPage + 1
        locationPresenter?.getLocations(page: currentPage, paramSort: nil, isAsc: nil)
    }
    
}

extension LocationController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LocationCell.cellIdentifier) as? LocationCell else{return UITableViewCell()}
        cell.configCell(location: locations[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    

    
}
extension LocationController:LocationViewable{
    func displayLocations(locations: [LocationResponse]) {
        DispatchQueue.main.async {
            PKHUD.sharedHUD.hide()
            self.refreshControl.endRefreshing()
            self.locations = locations
        }
        
    }
    
    func showAlert(message: String) {
        DispatchQueue.main.async {
            PKHUD.sharedHUD.hide()
            self.refreshControl.endRefreshing()
            UIUtilities.showSimpleAlert(title: nil, message: message, controller: self)
        }
        
    }
}

extension LocationController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.locationPresenter?.filterByWord(searchWord: searchText)
    }
}

extension LocationController:UIScrollViewDelegate{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)){
            self.getMoreLocations()
        }
        
    }
    
    
    
}
