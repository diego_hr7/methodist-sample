//
//  LocationCell.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/26/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol LocationCellViewable {
    func displayCell(locationName:NSAttributedString, airportImg:String, airportName:String, locationImg:String)
}

class LocationCell:UITableViewCell, LocationCellViewable{
    
    static let cellIdentifier = "LocationCellIdentifier"
    
    @IBOutlet weak var imgLocationPrev: UIImageView!
    @IBOutlet weak var lbNameLocation: UILabel!
    @IBOutlet weak var imgAirport: UIImageView!
    @IBOutlet weak var lbAirportName: UILabel!
    
    var cellPresenter:LocationCellPresentable!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(location:LocationResponse){
        self.cellPresenter = LocationCellPresenter(view: self)
        self.cellPresenter.setupLocaition(location: location)
        
    }
    
    func displayCell(locationName: NSAttributedString, airportImg: String, airportName: String, locationImg:String) {
        if self.lbNameLocation == nil || self.imgAirport == nil && self.lbAirportName == nil{return}
        self.lbNameLocation.attributedText = locationName
        self.imgAirport.image = UIImage(named: airportImg)
        self.lbAirportName.text = airportName
        
        //MARK: I don't know how to form the url :(
        
        
//        DispatchQueue.global(qos: .background).async { [weak self] in
//            guard let urlImage = URL(string: locationImg) else{return}
//            do{
//                let data = try Data(contentsOf: urlImage)
//                let thumbImage = UIImage(data: data)
//                DispatchQueue.main.async { [weak self] in
//                    self?.imgLocationPrev.image = thumbImage
//                }
//            }catch let error{
//                debugPrint("error:\(error.localizedDescription)")
//            }
//
//        }
        
    }
}
