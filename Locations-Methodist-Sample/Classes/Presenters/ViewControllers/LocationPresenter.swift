//
//  LocationPresenter.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/26/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

protocol Locationresentable {
    init(view: LocationViewable)
}

class LocationPresenter:Locationresentable{
    
    var view:LocationViewable!
    var locations:[LocationResponse] = []
    var locationsFiltered:[LocationResponse] = []
    private var currentFilterString:String?
    private var currentSort:String?
    private var currentOrderAsc:Bool? = true
    
    required init(view: LocationViewable){
        self.view = view
    }
    
    func filterByWord(searchWord:String?){
        if let search = searchWord{
            if search.isEmpty{
                currentFilterString = nil
                self.locationsFiltered = self.locations
                self.view.displayLocations(locations: locationsFiltered)
                
            }else{
                currentFilterString = search.lowercased()
                locationsFiltered = locations.filter{$0.name.lowercased().contains(search.lowercased())}
                self.view.displayLocations(locations: locationsFiltered)
            }
        }
    }
    
    func getLocations(page:Int = 1, paramSort:String?, isAsc:Bool?){
        if page == 1{
            self.locations = []
            self.locationsFiltered = []
            self.currentSort = paramSort
            self.currentOrderAsc = isAsc
        }
        Connection.shared.getLocations(page: page, paramSort: currentSort, isOrderAsc: currentOrderAsc) { (response, error) in
            if let error = error{
                self.view.showAlert(message: error.localizedDescription)
            }else if let locations = response as? [LocationResponse]{
                var locationsWithRules:[LocationResponse] = []
                locations.forEach { (currentLoc) -> () in
                    if !locationsWithRules.contains (where: {
                        $0.multiCarDisplayName == currentLoc.multiCarDisplayName || $0.name == "A4" || $0.name == "Q5"}) {
                        locationsWithRules.append(currentLoc)
                    }
                }
                if locationsWithRules.count>0{
                    self.locations.append(contentsOf: locationsWithRules)
                    if self.currentFilterString != nil{
                        self.filterByWord(searchWord: self.currentFilterString)
                    }else{
                        self.locationsFiltered = self.locations
                        self.view.displayLocations(locations: self.locationsFiltered)
                    }
                }else{
                    self.view.showAlert(message: "No more results")
                }
                
                
            }else{
                self.view.showAlert(message: "unable to parse the object")
            }
        }
    }
    
}
