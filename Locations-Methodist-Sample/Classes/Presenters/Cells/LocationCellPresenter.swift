//
//  LocationCellPresenter.swift
//  Locations-Methodist-Sample
//
//  Created by Armit Solutions on 10/26/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol LocationCellPresentable {
    init(view: LocationCellViewable)
    func setupLocaition(location:LocationResponse)
}

class LocationCellPresenter:LocationCellPresentable{

    private var viewCell:LocationCellViewable!
    
    var airportImage = ""
    var airportCode:String?
    var locationName = NSMutableAttributedString(string: "")
    var assetCode = ""
    
    required init(view: LocationCellViewable) {
        self.viewCell = view
    }
    
    func setupLocaition(location: LocationResponse) {
        locationName = NSMutableAttributedString(string: location.name)
        assetCode = location.assetCode
        airportCode = location.airportCode
        if let airpotCode = self.airportCode{
            self.airportCode = airpotCode
            airportImage = "airplane"
            let attrs = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0)]
            let airportCodeAtt = NSMutableAttributedString(string:"(" + airpotCode + ")", attributes:attrs)
            locationName.append(airportCodeAtt)
            
        }else{
            airportImage = "building"
        }
        self.viewCell.displayCell(locationName: locationName, airportImg: airportImage, airportName: location.description, locationImg: self.assetCode)
    }
    
}
