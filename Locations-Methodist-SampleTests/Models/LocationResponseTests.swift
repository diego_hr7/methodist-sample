//
//  LocationResponseTests.swift
//  Locations-Methodist-SampleTests
//
//  Created by Armit Solutions on 10/26/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Quick
import Nimble
@testable import Locations_Methodist_Sample

class LocationResponseTests: QuickSpec {
    override func spec(){
        
        var inputJson:String?
        
        beforeEach {
            inputJson = "[{\"id\": 2,\"name\": \"Austin\",\"airport_code\": \"AUS\",\"phone_number\": \"15126669680\",\"text_number\": \"15126669680\",\"fleet_type\": \"a4\",\"multi_car_display_name\": \"Austin\",\"open_date\": \"2013-02-28\",\"close_date\": null,\"gds_code\": \"AUSO01\",\"deliverable\": true,\"deliverable_description\": {\"description\": \"test 123\"},\"description\": \"Austin-Bergstrom International Airport\",\"time_zone\": \"America/Chicago\",\"bookable\": true,\"hours\": \"Mon - Sun 5:00am - 11:00pm\",\"asset_code\": \"aus\",\"address\": {\"id\": 3,\"line1\": \"2415 Highway 71 East\",\"line2\": null,\"city\": \"Del Valle\",\"state\": \"TX\",\"zip\": \"78617\",\"country\": \"US\",\"latitude\": \"30.214406\",\"longitude\": \"-97.660504\"},\"curbside_detail\": {\"active\": true,\"description\": \"test 123\"}},{\"id\": 101,\"name\": \"Boston\",\"airport_code\": \"BOS\",\"phone_number\": \"16173568182\",\"text_number\": \"16173568182\",\"fleet_type\": \"a4\",\"multi_car_display_name\": \"Boston\",\"open_date\": \"2019-04-15\",\"close_date\": null,\"gds_code\": \"BOSO01\",\"deliverable\": false,\"deliverable_description\": null,\"description\": \"Boston Logan International Airport\",\"time_zone\": \"America/New_York\",\"bookable\": true,\"hours\": \"Mon - Sun 5:00am - 11:00pm\",\"asset_code\": \"bos\",\"address\": {\"id\": 540517,\"line1\": \"235 Marginal Street\",\"line2\": null,\"city\": \"Chelsea\",\"state\": \"MA\",\"zip\": \"02150\",\"country\": \"US\",\"latitude\": \"42.38566\",\"longitude\": \"-71.027933\"},\"curbside_detail\": null}]"
        }
        
        describe("test for correct input json") {
            it("response must not be nil") {
                if let jsonString = inputJson, let data = jsonString.data(using: .utf8){
                    let responseObjct = LocationResponse.initWithData(data: data)
                    expect(responseObjct.0).notTo(beNil())
                    expect(responseObjct.1).to(beNil())
                }else{
                    fail()
                }
                
            }
            
            it("must be 2 locations") {
                if let jsonString = inputJson, let data = jsonString.data(using: .utf8){
                    let responseObjct = LocationResponse.initWithData(data: data)
                    if let locations = responseObjct.0{
                        expect(locations.count).to(equal(2))
                    }else{
                        fail()
                    }
                    
                }else{
                    fail()
                }
                
            }
        }
        
        describe("test for correct input json") {
            it("response must be nil") {
                let jsonInvalid = "<H1> Page not found</H1>"
                if let data = jsonInvalid.data(using: .utf8){
                    let responseObjct = LocationResponse.initWithData(data: data)
                    expect(responseObjct.0).to(beNil())
                    expect(responseObjct.1).notTo(beNil())
                }else{
                    fail()
                }
                
            }
            
        }
        
    }
    
    
}


//
