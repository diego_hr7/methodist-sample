//
//  LocationCellTests.swift
//  Locations-Methodist-SampleTests
//
//  Created by Armit Solutions on 10/26/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Quick
import Nimble

@testable import Locations_Methodist_Sample

class LocationCellTests: QuickSpec {
    override func spec(){
        
        var location:LocationResponse?
        var preseneter:LocationCellPresenter?
        
        beforeEach {
            location = LocationResponse(idLocation: 1, name: "Boston", airportCode: "ORD", phoneNumber: "", textNumber: "", fleetType: "", multiCarDisplayName: "", openDate: "", closeDate: "", gdsCode: "", deliverable: false, deliverableDescription: nil, description: "", timeZone: "", bookable: false, hours: "", assetCode: "image_plane", address: nil, curbside_detail: nil)
            preseneter = LocationCellPresenter(view: LocationCell())
        }
        
        describe("test correct config cell") {
            it("check the values against the presenter") {
                guard let location = location else{return}
                preseneter?.setupLocaition(location: location)
                expect(preseneter?.assetCode).to(equal(location.assetCode))
                expect(preseneter?.airportCode).to(equal(location.airportCode))
                expect(preseneter?.locationName.string).to(equal(location.name+"(ORD)"))
                expect(preseneter?.airportImage).to(equal("airplane"))
            }
        }
        
    }
}
