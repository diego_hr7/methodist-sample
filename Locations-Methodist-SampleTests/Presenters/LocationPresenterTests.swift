//
//  LocationPresenterTests.swift
//  Locations-Methodist-SampleTests
//
//  Created by Armit Solutions on 10/26/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Quick
import Nimble
@testable import Locations_Methodist_Sample

class LocationPresenterTests: QuickSpec {
    override func spec(){
        var presenter:LocationPresenter!
        var view:LocationViewable!
        
        let mockData = [LocationResponse(idLocation: 1, name: "Boston", airportCode: nil, phoneNumber: "", textNumber: "", fleetType: "", multiCarDisplayName: "", openDate: "", closeDate: "", gdsCode: "", deliverable: false, deliverableDescription: nil, description: "", timeZone: "", bookable: false, hours: "", assetCode: "", address: nil, curbside_detail: nil), LocationResponse(idLocation: 2, name: "Chicago", airportCode: nil, phoneNumber: "", textNumber: "", fleetType: "", multiCarDisplayName: "", openDate: "", closeDate: "", gdsCode: "", deliverable: false, deliverableDescription: nil, description: "", timeZone: "", bookable: false, hours: "", assetCode: "", address: nil, curbside_detail: nil)]
            
        beforeEach {
            view = LocationController()
            presenter = LocationPresenter(view: view)
        }
        describe("test for location presenter") {
            it("cheeck view's presenter init must not be nil") {
                expect(presenter.view).notTo(beNil())
            }
            it("should contain 1 airport") {
                presenter.locations = mockData
                presenter.filterByWord(searchWord: "bos")
                expect(presenter.locationsFiltered.count).to(equal(1))
            }
            it("should contain 0 airport") {
                presenter.locations = mockData
                presenter.filterByWord(searchWord: "houston")
                expect(presenter.locationsFiltered.count).to(equal(0))
            }
            
        }
    }
}

